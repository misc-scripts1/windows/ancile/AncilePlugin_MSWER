# Ancile Windows Error Reporting System
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSWER

Ancile Microsoft Error Reporting System plugin disables Microsoft's Error Reporting service.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile